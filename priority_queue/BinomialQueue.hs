module BinomialQueue(BinomialQueue(..)) where

import Queue
import BinomialTree
import Data.List (minimumBy, deleteBy)

data BinomialQueue a = BQueue [Tree a] 
    deriving (Show)

ins :: (Ord a) => Tree a -> BinomialQueue a -> BinomialQueue a
ins x (BQueue []) = BQueue [x]
ins n1 (BQueue (n2 : xs)) = 
  if (rank n1 < rank n2) then BQueue (n1 : n2 : xs)
    else ins (link n1 n2) (BQueue xs)


meld :: (Ord a) => [Tree a] -> [Tree a] -> [Tree a]
meld [] t2 = t2
meld t1 [] = t1
meld (x : t1) (y : t2)  
  | (rank x < rank y) = x : (meld t1 (y : t2))
  | rank x > rank y   = y : (meld (x : t1) t2)
  | otherwise         = (link x y) : (meld t1 t2)


instance PriorityQueue BinomialQueue where
    empty = BQueue []

    isEmpty (BQueue []) = True
    isEmpty x = False

    insert x q = ins (Node x 0 []) q

    findMin (BQueue []) = Nothing
    findMin (BQueue ts) = Just $ val $ getMin ts

    deleteMin (BQueue []) = Nothing
    deleteMin q@(BQueue tree) = Just $ BQueue $ meld (reverse c) ts
     where
        delMin [t] = (t, [])
        delMin (t:ts) = 
          let (t', ts') = delMin ts
          in if (val t <= val t') then (t, ts) else (t', t : ts')
        (Node x r c, ts) = delMin tree
       

    merge (BQueue t1) (BQueue t2) = BQueue $ meld t1 t2
