module BinomialTree where

import Data.List (minimumBy) 


link :: (Ord a) => Tree a -> Tree a -> Tree a
link t1@(Node e1 r1 c1) t2@(Node e2 r2 c2) = 
  if (e1 <= e2) then Node e1 (r1 + 1) (t2 : c1)
  else Node e2 (r2 + 1) (t1 : c2)

getMin :: (Ord a) => [Tree a]  -> Tree a
getMin ts = minimumBy (\(Node x _ _) (Node y _ _) -> compare x y) ts

data Tree a = Node {val :: a, rank :: Int, children :: [Tree a]}
    deriving (Show)

