{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE UndecidableInstances #-}

module BootstrappedQueue where

import Queue


data Ra q a = PRa {x :: a, queue :: q (Ra q a)}

data BootstrappedQ q a = Nil | BQ {p :: Ra q a}

deriving instance (Show a, Show (q (Ra q a)), Show (BootstrappedQ q a)) => Show ( (Ra q a))
deriving instance Show (Ra q a) => Show (BootstrappedQ q a)

instance (Eq a) => Eq (Ra q a) where
    (==) (PRa x _) (PRa y _) = x == y
 
instance (Ord a) => Ord (Ra q a) where
    (<=) (PRa x _) (PRa y _) = x <= y 
    
instance (PriorityQueue q) => PriorityQueue (BootstrappedQ q)  where
    empty = Nil

    isEmpty Nil = True
    isEmpty _ = False


    insert x q = merge (BQ (PRa x empty)) q

    merge Nil q = q
    merge q Nil = q
    merge (BQ r1@(PRa x1 q1)) (BQ r2@(PRa x2 q2)) 
      | x1 <= x2 = BQ (PRa x1 (insert r2 q1))
      | otherwise = BQ (PRa x2 (insert r1 q2))

    findMin Nil = Nothing
    findMin (BQ (PRa x q)) = Just x
    
    deleteMin Nil = Nothing
    deleteMin (BQ (PRa x q)) 
        | isEmpty q = Just empty
        | otherwise = do
            (PRa y q1) <- findMin q
            q2 <- deleteMin q
            return $ BQ $ PRa y (merge q1 q2)
