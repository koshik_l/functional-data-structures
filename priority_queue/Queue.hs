module Queue where

class PriorityQueue q where
    insert :: (Ord a) => a -> q a -> q a
    deleteMin :: (Ord a) => q a -> Maybe (q a)
    findMin :: (Ord a) => q a -> Maybe a
    empty :: q a
    isEmpty :: q a -> Bool
    merge :: (Ord a) => q a -> q a -> q a
