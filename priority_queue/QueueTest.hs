{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleInstances#-}
{-# LANGUAGE ScopedTypeVariables #-}

module QueuesTest where

import Test.QuickCheck
import Test.QuickCheck.All
import Queue
import BinomialQueue
import SkewBinomialQueue
import RootedQueue
import BootstrappedQueue
import Data.List (sort)
import Data.Maybe (catMaybes)
import Data.Proxy

arbitrary' :: (Ord a, Arbitrary a, PriorityQueue q) => Gen (q a)
arbitrary' =  do
      elems <- arbitrary :: (Arbitrary a) => Gen [a]
      return $ foldr insert empty (elems)


min_of_merged :: (Ord a, PriorityQueue q) => q a -> q a -> Bool
min_of_merged q1 q2 = (min m1 m2 == Nothing) || (m == (min m1 m2))
  where 
          m1 = findMin q1
          m2 = findMin q2
          m  = findMin $ merge q1 q2

toList :: (Ord a, PriorityQueue q) => Maybe (q a) -> [Maybe a] -> [Maybe a]
toList (Just q) acc = 
  if (isEmpty q) 
    then acc
    else toList (deleteMin q) ((findMin q) : acc)
        
isSorted l = all (\(x, y) -> x >= y) $ zip l (tail l)

delete_min :: (Ord a, PriorityQueue q) => q a -> Bool
delete_min q = isSorted $ toList (Just q) []


insert_test :: forall a proxy q. (PriorityQueue q, Ord a) => proxy q -> [a] -> Bool
insert_test _ l = 
  let sorted = catMaybes $ toList (Just $ foldr insert (empty :: q a) l)  []
  in reverse sorted == sort l
 
instance (Ord a, Arbitrary a) => Arbitrary (BinomialQueue a) where
    arbitrary = arbitrary'

prop_min_of_merged_bq :: BinomialQueue Int -> BinomialQueue Int -> Bool
prop_min_of_merged_bq = min_of_merged

prop_delete_min_bq :: BinomialQueue Int -> Bool
prop_delete_min_bq = delete_min

prop_insert_test_bq :: [Int] -> Bool
prop_insert_test_bq = insert_test (Proxy :: Proxy BinomialQueue)

instance (Ord a, Arbitrary a) => Arbitrary (SkewBinomialQueue a) where
    arbitrary = arbitrary'

prop_min_of_merged_skew :: SkewBinomialQueue Int -> SkewBinomialQueue Int -> Bool
prop_min_of_merged_skew = min_of_merged

prop_delete_min_skew :: SkewBinomialQueue Int -> Bool
prop_delete_min_skew = delete_min

prop_insert_test_skew :: [Int] -> Bool
prop_insert_test_skew = insert_test (Proxy :: Proxy SkewBinomialQueue)

instance (Ord a, Arbitrary a, PriorityQueue q) => Arbitrary (RootedQueue q a) where
    arbitrary = arbitrary'

type RootedSkewQueue = RootedQueue SkewBinomialQueue

type RtSkewInt = RootedSkewQueue Int

prop_min_of_merged_rt :: RtSkewInt -> RtSkewInt -> Bool
prop_min_of_merged_rt = min_of_merged

prop_delete_min_rt :: RtSkewInt -> Bool
prop_delete_min_rt = delete_min

prop_insert_test_rt :: [Int] -> Bool
prop_insert_test_rt = insert_test (Proxy :: Proxy RootedSkewQueue)

instance (Ord a, Arbitrary a, PriorityQueue (q )) => Arbitrary (BootstrappedQ (q ) a) where
    arbitrary = arbitrary'

type BRtSkewInt = BootstrappedQ RootedSkewQueue Int 

prop_min_of_merged_b :: BRtSkewInt -> BRtSkewInt -> Bool
prop_min_of_merged_b = min_of_merged

prop_delete_min_b :: BRtSkewInt -> Bool
prop_delete_min_b = delete_min

prop_insert_test_b :: [Int] -> Bool
prop_insert_test_b = insert_test (Proxy :: Proxy (BootstrappedQ RootedSkewQueue))

return []
runTests :: IO Bool
runTests = $quickCheckAll
