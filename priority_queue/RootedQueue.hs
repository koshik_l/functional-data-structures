module RootedQueue where

import Queue 


data RootedQueue b a = Empty | RQueue {r :: a, q :: (b a)}
    deriving (Show)

instance (PriorityQueue b) => PriorityQueue (RootedQueue (b ))  where
    empty = Empty

    isEmpty Empty = True
    isEmpty x = False

    insert x Empty = RQueue x empty
    insert x (RQueue e q) 
      | x <= e     = RQueue x (insert e q)
      | otherwise = RQueue e (insert x q) 

    findMin Empty = Nothing
    findMin (RQueue e q) = Just e
    
    deleteMin Empty = Nothing
    deleteMin (RQueue e q) 
        | isEmpty q = Just empty
        | otherwise = do
            newMin <- findMin q
            newQ <- deleteMin q
            return $ RQueue newMin newQ
 
    merge Empty q = q
    merge q Empty = q
    merge (RQueue e1 q1) (RQueue e2 q2)
      | e1 <= e2 = RQueue e1 (merge q1 (insert e2 q2))
      | otherwise = RQueue e2 (merge (insert e1 q1) q2)
