module SkewBinomialQueue where

import Queue
import BinomialTree
import Data.List (minimumBy, deleteBy)


data SkewBinomialQueue a = Skew [Tree a]
    deriving (Show)

insSkew :: (Ord a) => Tree a -> [Tree a] -> [Tree a]
insSkew x [] = [x]
insSkew n1 (n2 : xs) = 
  if (rank n1 < rank n2) then (n1 : n2 : xs)
  else insSkew (link n1 n2) xs

skewLink t0@(Node x0 r0 c0) t1@(Node x1 r1 c1) t2@(Node x2 r2 c2) 
  | (x1 <= x0 && x1 <= x2) = Node x1 (r1 + 1) (t0 : t2 : c1)
  | x2 <= x0 && x2 <= x1   = Node x2 (r2 + 1) (t0 : t1 : c2)
  | otherwise            = Node x0 (r1 + 1) [t1, t2] 

uniqify :: (Ord a) => [Tree a] -> [Tree a]
uniqify [] = []
uniqify (t : ts) = insSkew t ts

meldUniq :: (Ord a) => [Tree a] -> [Tree a] -> [Tree a]
meldUniq [] q = q
meldUniq q [] = q
meldUniq a@(t1 : ts1) b@(t2 : ts2) 
  | rank t1 < rank t2 = t1 : (meldUniq ts1 b)
  | rank t2 < rank t1 = t2 : (meldUniq  a ts2)
  | otherwise         = insSkew (link t1 t2) (meldUniq ts1 ts2)

instance PriorityQueue SkewBinomialQueue where
    empty = Skew []

    isEmpty (Skew []) = True
    isEmpty x = False

    insert x (Skew t@(t1 : t2 : ts)) = 
      if rank t1 == rank t2 then Skew $ (skewLink (Node x 0 []) t1 t2) : ts
      else Skew $ (Node  x 0 []) : t
    insert x (Skew ts) = Skew $ (Node x 0 []) : ts

    findMin (Skew []) = Nothing
    findMin (Skew ts) = Just $ val $ getMin ts
    
    deleteMin (Skew []) = Nothing
    deleteMin q@(Skew trees) = Just $ foldr insert (merge (Skew ts) (Skew ts')) xs'
      where
        split ts xs [] = (ts, xs)
        split ts xs (t : c) = 
          if (rank t == 0) then split ts ((val t) : xs) c
          else split (t : ts) xs c 
        
        delMin [t] = (t, [])
        delMin (t:ts) = 
          let (t', ts') = delMin ts
          in if (val t <= val t') then (t, ts) else (t', t : ts')

        (Node x r c, ts) = delMin trees
        (ts', xs') = split [] [] c

    merge (Skew t1) (Skew t2) = Skew $ meldUniq (uniqify t1) (uniqify t2)
